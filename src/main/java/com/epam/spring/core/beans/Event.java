package com.epam.spring.core.beans;

import java.text.DateFormat;
import java.util.Date;

public class Event {

    private String id;
    private String msg;
    private Date date;
    private DateFormat df;

    @Override
    public String toString() {
        return "Event{" +
                "id='" + id + '\'' +
                ", msg='" + msg + '\'' +
                ", date=" + date +
                '}';
    }
}
